import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.Console;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.Color;
import java.awt.Font;



public class GUI {

	private JFrame frmSklep;
	JTextArea koszyk_area;
	
	Koszyk Zakupy = new Koszyk();
	ArrayList<magazyn>lista = new ArrayList<magazyn>();
	String koszTekst="";
	
	magazyn wybor;
	String nazwa;
	double cena;
	
	
	magazyn[] mag = {new magazyn("Budweiser",1.50,10),new magazyn("Millers",2.00,20), new magazyn("Coors",5.00,7),};
	private JTextField suma_textField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmSklep.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws FileNotFoundException 
	 */
	public GUI() throws FileNotFoundException {
		try {
			pobierzDane();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		konwertujNaTab();
		initialize();
	}

		
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSklep = new JFrame();
		frmSklep.setTitle("Sklep");
		frmSklep.getContentPane().setBackground(Color.GRAY);
		frmSklep.setBounds(100, 100, 611, 427);
		frmSklep.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSklep.getContentPane().setLayout(null);
		
		JComboBox comboBox = new JComboBox(mag);
		comboBox.setSelectedIndex(1);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				String co = comboBox.getSelectedItem().toString();
				String[] temp = co.split("\t");
				nazwa = temp[0];
				cena= Double.parseDouble(temp[1]);
			}
		});
		comboBox.setEditable(true);
		comboBox.setBounds(117, 43, 242, 20);
		frmSklep.getContentPane().add(comboBox);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spinner.setBounds(369, 43, 40, 20);
		frmSklep.getContentPane().add(spinner);
		
		JButton btnDoKoszyka = new JButton("Do koszyka");
		btnDoKoszyka.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				
				
				int ilosc = (int) spinner.getValue();
				
				int temp=-1;
				//pomniejszenie magazynu o ilosc
				for(int i=0; i<lista.size(); i++)
					if(nazwa.equals(lista.get(i).getNazwa()))
						temp=i;
				
				if(ilosc>lista.get(temp).getIlosc())
					JOptionPane.showMessageDialog(null, "Nie mamy tyle dostepnych!");
				else
				{
					koszyk_area.setText("");
					Zakupy.dodajDoKoszyka(nazwa, cena, ilosc);
					koszTekst += Zakupy.toString();
					koszyk_area.append(koszTekst);
				
					String konwerterDoubleNaString = Double.toString(Zakupy.getCena());
					suma_textField.setText(konwerterDoubleNaString);
				
					lista.get(temp).setIlosc(ilosc);
				}
				
				
			}
		});
		btnDoKoszyka.setBounds(419, 42, 109, 23);
		frmSklep.getContentPane().add(btnDoKoszyka);
		
		suma_textField = new JTextField();
		suma_textField.setBounds(461, 322, 86, 20);
		frmSklep.getContentPane().add(suma_textField);
		suma_textField.setColumns(10);
		
		JLabel lblSuma = new JLabel("Suma");
		lblSuma.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSuma.setForeground(Color.WHITE);
		lblSuma.setBounds(406, 325, 46, 14);
		frmSklep.getContentPane().add(lblSuma);
		
		koszyk_area = new JTextArea();
		koszyk_area.setBounds(76, 117, 472, 183);
		frmSklep.getContentPane().add(koszyk_area);
		
		JLabel lblNazwa = new JLabel("Nazwa");
		lblNazwa.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNazwa.setForeground(Color.WHITE);
		lblNazwa.setBounds(76, 96, 46, 14);
		frmSklep.getContentPane().add(lblNazwa);
		
		JLabel lblCena = new JLabel("Cena");
		lblCena.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCena.setForeground(Color.WHITE);
		lblCena.setBounds(162, 96, 46, 14);
		frmSklep.getContentPane().add(lblCena);
		
		JLabel lblcznie = new JLabel("\u0141\u0105cznie");
		lblcznie.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblcznie.setForeground(Color.WHITE);
		lblcznie.setBounds(501, 93, 46, 14);
		frmSklep.getContentPane().add(lblcznie);
		
		JLabel lblIlo = new JLabel("Ilo\u015B\u0107");
		lblIlo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblIlo.setForeground(Color.WHITE);
		lblIlo.setBounds(419, 93, 46, 14);
		frmSklep.getContentPane().add(lblIlo);
		
	}

	public magazyn pobierzDane() throws IOException
	{
		FileReader fr=null;
		String linia ="";
		magazyn temp=null;

		//otwieramy plik
		try
		{
			fr = new FileReader("sklep.txt");
		}
		catch (FileNotFoundException e)
		{
			System.out.println("B��D PRZY OTWIERANIU PLIKU!");
		}
		
		//pokolei wczytujemy linie. Jedna linia - jeden produkt.
		BufferedReader bfr = new BufferedReader(fr);
		try 
		{
			while((linia = bfr.readLine()) != null)
			{
				String[] slowa = linia.split(";");
				lista.add(new magazyn(slowa[0],Double.parseDouble(slowa[1]),Integer.parseInt(slowa[2])));
		    }
		} 
		catch (IOException e) 
		{
			System.out.println("B��D ODCZYTU Z PLIKU!");
		}
		
		bfr.close();
		
		
		return temp;
	}
	
	public void konwertujNaTab()
	{
		mag = new magazyn[lista.size()-1];
		
		for (int i=0; i<lista.size();i++)
		{
			mag=lista.toArray(new magazyn[lista.size()]);
		}
	}
}
