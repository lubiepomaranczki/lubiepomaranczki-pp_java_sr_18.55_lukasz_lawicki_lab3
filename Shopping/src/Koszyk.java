// **********************************************************************
//   Koszyk.java
//
//   Reprezentuje koszyk zakupowy jako tablica przedmiotow (Items)
// **********************************************************************

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;


public class Koszyk
{

    private static int iloscRzeczy;       // ilosc rzeczy w koszyku
    private double cenaCalkowita;  // calkowita cena przedmiotow w koszyku
    public ArrayList<Item> koszyk = new ArrayList<Item>();			//deklaracja tablicy obiekt�w Item 
    
    // -----------------------------------------------------------
    // tworzy pusty koszyk o pojemnosci 5 przedmiotow.
    // -----------------------------------------------------------
    public Koszyk()
    {
    	iloscRzeczy = 0;
    	cenaCalkowita = 0.0;
    }

    // -------------------------------------------------------
    //  Dodaje rzecz do koszyka.
    // -------------------------------------------------------
    public void dodajDoKoszyka(String nazwaRzeczy, double cena, int ilosc)
    {
    	koszyk.add(new Item(nazwaRzeczy, cena, ilosc));
    	iloscRzeczy++;
    	//uaktualnienie ceny
    	cenaCalkowita += (cena*ilosc);
    }

    // -------------------------------------------------------
    //  Zwraca zawartosc koszyka wraz z podsumowaniem.
    // -------------------------------------------------------
    public String toString()
    {
    	NumberFormat fmt = NumberFormat.getCurrencyInstance();

    	String zawartosc = "";

    	zawartosc = koszyk.get(iloscRzeczy-1).toString() + "\n";

    	return zawartosc;
    }
    
    public static ArrayList<Item> pobierzDane() throws FileNotFoundException	
    {
    	ArrayList<Item>sklep = new ArrayList<Item>();
		
		File plik = new File("sklep.txt");
		Scanner skaner = new Scanner(plik);
		
		while(skaner.hasNextLine())
		{
			String linia = skaner.nextLine();
			String[] slowa = linia.split(";");
			sklep.add(new Item(slowa[0],Double.parseDouble(slowa[1]),Integer.parseInt(slowa[2])));
		}
		
		skaner.close();
		
		
		return sklep;
	}
    
    public double getCena()
    {
	return cenaCalkowita;
    }

}
