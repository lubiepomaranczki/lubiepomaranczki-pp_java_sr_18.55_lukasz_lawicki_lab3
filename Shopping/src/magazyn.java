class magazyn
{
  String name;
  double cena;
  int ilosc;
  public magazyn(String n, double v, int i)
  {
    name = n; cena = v; ilosc=i;
  }

  
  public String toString()
  {
	  String produkt = name + "\t" + String.valueOf(cena);
	  return produkt;
  }
  
  public String getNazwa()
  {
	return name;
  }
  
  public int getIlosc()
  {
	return ilosc;
  }
  
  public void setIlosc(int ileKupionych)
  {
	  ilosc=ilosc-ileKupionych;
  }
}